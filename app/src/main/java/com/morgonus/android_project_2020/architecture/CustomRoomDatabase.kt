package com.morgonus.android_project_2020.architecture

import androidx.room.Database
import androidx.room.RoomDatabase
import com.morgonus.android_project_2020.dao.PlaylistDao
import com.morgonus.android_project_2020.dao.WifiValueDao
import com.morgonus.android_project_2020.model.Playlist
import com.morgonus.android_project_2020.model.WifiValue

@Database(
    entities = [
        WifiValue::class,
        Playlist::class
    ],
    version = 4,
    exportSchema = false
)
abstract class CustomRoomDatabase : RoomDatabase() {

    abstract fun mWifiValueDao(): WifiValueDao
    abstract fun mPlaylistDao(): PlaylistDao
}
