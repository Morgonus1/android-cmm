package com.morgonus.android_project_2020.repository

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import com.morgonus.android_project_2020.architecture.CustomApplication
import com.morgonus.android_project_2020.model.Playlist
import kotlinx.android.synthetic.main.activity_spotify.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class PlaylistRepository{
    private val mPlaylistDao = CustomApplication.instance.mApplicationDatabase.mPlaylistDao()

    private var apps: ArrayList<Playlist>? = null

    fun selectAllPlaylist() : LiveData<List<Playlist>> {
        return mPlaylistDao.selectAll()
    }

    private suspend fun insertPlaylist(playlist: Playlist) = withContext(Dispatchers.IO) {
        Log.e("a",playlist.name)
        mPlaylistDao.insert(playlist)
    }

    suspend fun fetchData(access: String) {
        apps = ArrayList();
        val request  = Request.Builder()
            .url("https://api.spotify.com/v1/me/playlists")
            .addHeader("Authorization", "Bearer $access")
            .build();

        val client = OkHttpClient();
        val mCall = client.newCall(request);
        mCall.enqueue(object : Callback {
            override fun onFailure(call: Call?, e: IOException) {
                Log.e("a", e.message.toString());
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call?, response: Response) {
                try {
                    val body = response.body()
                    val responseBody = response.body()!!.string()
                    val jsonObject = JSONObject(responseBody);
                    val postsArray: JSONArray = jsonObject.getJSONArray("items")

                    for (i in 0 until postsArray.length()) {
                        val Object: JSONObject = postsArray.getJSONObject(i)
                        val name = Object.getString("name");
                        val image = Object.getJSONArray("images");
                        val imageObject = image.getJSONObject(0);
                        val imageUrl = imageObject.getString("url");
                        val playlist = Playlist(name, imageUrl, access);
                        mPlaylistDao.insert(playlist);
                    }

                } catch (e: JSONException) {
                    Log.e("a", e.message.toString());
                }
            }
        })
    }
}