package com.morgonus.android_project_2020.repository

import android.content.Context
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import androidx.lifecycle.LiveData
import com.morgonus.android_project_2020.architecture.CustomApplication
import com.morgonus.android_project_2020.model.WifiValue

class SensorRepository  {

    private var wifiManager = CustomApplication.instance.getSystemService(Context.WIFI_SERVICE) as WifiManager
    private val mWifiRepository = CustomApplication.instance.mApplicationDatabase.mWifiValueDao()

    fun getLevelWifi(): Int{
        return wifiManager.calculateSignalLevel(wifiManager.connectionInfo.rssi);
    }

    fun getWifiEnabled(): Boolean {
        return wifiManager.isWifiEnabled;
    }

    fun selectAll(): LiveData<List<WifiValue>> {
        return mWifiRepository.selectAll()

    }

    fun deleteAll() {
        mWifiRepository.deleteAll();
    }

    fun insert() {
        val mWifiValue = WifiValue(this.getWifiEnabled(),this.getLevelWifi())
        mWifiRepository.insert(mWifiValue);
    }
}