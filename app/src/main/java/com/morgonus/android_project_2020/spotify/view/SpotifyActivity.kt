package com.morgonus.android_project_2020.spotify.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.morgonus.android_project_2020.R
import com.morgonus.android_project_2020.model.Playlist
import com.morgonus.android_project_2020.view.adapter.PlaylistAdapter
import com.morgonus.android_project_2020.viewmodel.PlayListViewModel
import com.spotify.sdk.android.auth.AuthorizationClient
import com.spotify.sdk.android.auth.AuthorizationRequest
import com.spotify.sdk.android.auth.AuthorizationResponse
import kotlinx.android.synthetic.main.activity_spotify.*
import okhttp3.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException


class SpotifyActivity : AppCompatActivity() {
    private val REDIRECT_URI = "login://callback"
    val AUTH_TOKEN_REQUEST_CODE = 0x10
    val AUTH_CODE_REQUEST_CODE = 0x11
    private var mAccessToken: String? = null
    private var apps: ArrayList<Playlist>? = null

    private lateinit var mAdapter: PlaylistAdapter
    private lateinit var mViewModel: PlayListViewModel

    private var mObserverPlaylist = Observer<List<Playlist>> {
        updateRecyclerView(ArrayList(it))
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spotify)

        apps = ArrayList();
        mAdapter = PlaylistAdapter(this);
        recyclerView.layoutManager = GridLayoutManager(this, 2,
                RecyclerView.VERTICAL, false)
        recyclerView.adapter = mAdapter

        login_spotify.setOnClickListener{ login() }
        get_playlist.setOnClickListener{ getPlaylist() }
        mViewModel = ViewModelProvider(this)[PlayListViewModel::class.java]

    }

    private fun getPlaylist(){
        mViewModel.fetchNewQuote(mAccessToken!!);
        recyclerView.visibility = View.VISIBLE;
        get_playlist.visibility = View.GONE;
    }

    override fun onStart() {
        super.onStart()
        mViewModel.mPlayListLiveData.observe(this, mObserverPlaylist)

    }
    override fun onStop() {
        mViewModel.mPlayListLiveData.removeObserver(mObserverPlaylist)
        super.onStop()
    }

    private fun updateRecyclerView(newList: ArrayList<Playlist>) {
        mAdapter.rebuild(newList)

    }

    private fun login(){

        val request = getAuthenticationRequest(AuthorizationResponse.Type.TOKEN)
        AuthorizationClient.openLoginActivity(this, AUTH_TOKEN_REQUEST_CODE, request);
    }
    private fun getAuthenticationRequest(type: AuthorizationResponse.Type): AuthorizationRequest? {
        return AuthorizationRequest.Builder("0baa0f35824340aeb9624c6b5528dbab", type, REDIRECT_URI)
                .setShowDialog(false)
                .setScopes(arrayOf("user-read-email"))
                .build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val response = AuthorizationClient.getResponse(resultCode, data)
        if (response.error != null && response.error.isEmpty()) {
            Log.e("error", response.error)
        }
        if (requestCode == AUTH_TOKEN_REQUEST_CODE) {
            Log.e("accessToken", response.accessToken)
            mAccessToken = response.accessToken;
            login_spotify.visibility = View.GONE;
            get_playlist.visibility = View.VISIBLE;


        } else if (requestCode == AUTH_CODE_REQUEST_CODE) {
            Log.e("code", response.code)
        }
    }

}