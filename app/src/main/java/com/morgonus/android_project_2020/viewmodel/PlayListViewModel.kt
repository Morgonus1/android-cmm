package com.morgonus.android_project_2020.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.morgonus.android_project_2020.model.Playlist
import com.morgonus.android_project_2020.repository.PlaylistRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
class PlayListViewModel : ViewModel() {
    private val mPlaylistRepository: PlaylistRepository by lazy { PlaylistRepository() }
    var mPlayListLiveData: LiveData<List<Playlist>> = mPlaylistRepository.selectAllPlaylist()

    fun fetchNewQuote(access:String) {
        viewModelScope.launch(Dispatchers.IO) {
            mPlaylistRepository.fetchData(access);
        }
    }

}