package com.morgonus.android_project_2020.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.morgonus.android_project_2020.model.WifiValue
import com.morgonus.android_project_2020.repository.SensorRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WifiViewModel : ViewModel() {
    private val mWifiRepository: SensorRepository by lazy { SensorRepository() }
    var liveData: LiveData<List<WifiValue>> = mWifiRepository.selectAll();

    fun add(){
        viewModelScope.launch(Dispatchers.IO) {
            mWifiRepository.insert();
        }
    }
    fun deleteAll(){
        viewModelScope.launch(Dispatchers.IO) {
            mWifiRepository.deleteAll()
        }
    }
}
