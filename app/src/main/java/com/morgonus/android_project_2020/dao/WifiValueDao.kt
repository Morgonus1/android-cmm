package com.morgonus.android_project_2020.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.morgonus.android_project_2020.model.WifiValue

@Dao
interface WifiValueDao {
    @Query("SELECT * FROM wifi ")
    fun selectAll(): LiveData<List<WifiValue>>

    @Insert()
    fun insert(wifiValue: WifiValue)

    @Query("DELETE FROM wifi")
    fun deleteAll()
}
