package com.morgonus.android_project_2020.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.morgonus.android_project_2020.model.Playlist

@Dao
interface PlaylistDao {
    @Query("SELECT * FROM playlist ")
    fun selectAll(): LiveData<List<Playlist>>

    @Insert()
    fun insert(wifiValue: Playlist)

    @Query("DELETE FROM wifi")
    fun deleteAll()
}
