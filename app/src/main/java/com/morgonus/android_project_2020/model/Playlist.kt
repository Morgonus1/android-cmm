package com.morgonus.android_project_2020.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "playlist")
data class Playlist(val name : String, val image: String,val accessToken: String) {
    @PrimaryKey(autoGenerate = true)
    var id : Long = 0
}