package com.morgonus.android_project_2020.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "wifi")
data class WifiValue(val wifiEnabled : Boolean,val levelWifi:Int) {
    @PrimaryKey(autoGenerate = true)
    var id : Long = 0
}