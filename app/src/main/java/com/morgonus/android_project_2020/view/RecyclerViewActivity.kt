package com.morgonus.android_project_2020.view

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.morgonus.android_project_2020.R
import com.morgonus.android_project_2020.view.adapter.WifiAdapter
import com.morgonus.android_project_2020.viewmodel.WifiViewModel
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.morgonus.android_project_2020.model.WifiValue
import kotlinx.android.synthetic.main.activity_recycler_view.*


class RecyclerViewActivity : AppCompatActivity(){

    companion object {
        const val PERMISSION_WIFI_PERMISSION = 42
    }

    private lateinit var mAdapter: WifiAdapter
    private lateinit var mViewModel: WifiViewModel
    private var mObserver = Observer<List<WifiValue>> {
        updateRecyclerView(ArrayList(it))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view)

        mViewModel = ViewModelProvider(this)[WifiViewModel::class.java]
        mAdapter = WifiAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = mAdapter
        buttonAddItem.setOnClickListener { add() }
        buttonDeleteItem.setOnClickListener { deleteAll() }


    }

    override fun onStart() {
        super.onStart()
        mViewModel.liveData.observe(this, mObserver)
    }

    override fun onStop() {
        mViewModel.liveData.removeObserver(mObserver)
        super.onStop()
    }

    private fun add() {
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_WIFI_STATE), PERMISSION_WIFI_PERMISSION)
    }

    private fun updateRecyclerView(newList: ArrayList<WifiValue>) {
        mAdapter.rebuild(newList)
    }

    private fun deleteAll() {
        mViewModel.deleteAll()
    }


    private fun checkIfPermissionIsGranted(permissionName: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permissionName) == PackageManager.PERMISSION_GRANTED
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mViewModel.add();
        }
    }
}
