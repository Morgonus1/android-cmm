package com.morgonus.android_project_2020.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.morgonus.android_project_2020.R
import com.morgonus.android_project_2020.architecture.CustomApplication
import com.morgonus.android_project_2020.model.WifiValue

class WifiAdapter (private val context : Context) : RecyclerView.Adapter<WifiAdapter.ViewHolder>() {

    private val mWifi = ArrayList<WifiValue>()
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // This line perform the matching with our ViewHolder and the item from layout
        return ViewHolder(mInflater.inflate(R.layout.item_custom_recycler, parent, false))
    }
    override fun getItemCount(): Int  = mWifi.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // onBindViewHolder is called for each item we want to display so we need to get each object
        val currentItem = mWifi[position]

        if(currentItem.wifiEnabled) {
            val state = CustomApplication.instance.getString(R.string.wifi_enabled);
            val level = state+" : "+currentItem.levelWifi.toString();
            holder.state.text = level;
            holder.icon.setImageResource(R.drawable.wifi_on)
        }
        else {
            holder.state.text = CustomApplication.instance.getString(R.string.wifi_disabled);
            holder.icon.setImageResource(R.drawable.wifi_off);
        }
    }

    fun rebuild(wifiValue: ArrayList<WifiValue>) {
        // This is the simplest way to update the list
        mWifi.clear()
        mWifi.addAll(wifiValue)
        // Needed to said to recycler view we have new data
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val state: TextView = itemView.findViewById(R.id.name)
        val icon: ImageView = itemView.findViewById(R.id.image);
    }
}
