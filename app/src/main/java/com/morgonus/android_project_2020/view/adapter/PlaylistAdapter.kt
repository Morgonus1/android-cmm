package com.morgonus.android_project_2020.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaderFactory
import com.bumptech.glide.load.model.LazyHeaders
import com.morgonus.android_project_2020.R
import com.morgonus.android_project_2020.model.Playlist
import com.morgonus.android_project_2020.model.WifiValue


class PlaylistAdapter(private val context: Context) : RecyclerView.Adapter<PlaylistAdapter.ViewHolder>() {

    private val mPlaylist = ArrayList<Playlist>()
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // This line perform the matching with our ViewHolder and the item from layout
        return ViewHolder(mInflater.inflate(R.layout.item_custom_playlist, parent, false))
    }
    override fun getItemCount(): Int  = mPlaylist.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // onBindViewHolder is called for each item we want to display so we need to get each object
        val currentItem = mPlaylist[position]


        val glideUrl = GlideUrl(
            "url", LazyHeaders.Builder()
                .addHeader("Authorization", "Bearer: "+currentItem.accessToken)
                .build()
        )
        holder.state.text = currentItem.name
        Glide.with(context)
            .load(glideUrl)
            .centerCrop().into(holder.icon);
    }

    fun rebuild(playlist: ArrayList<Playlist>) {
        // This is the simplest way to update the list
        mPlaylist.clear()
        mPlaylist.addAll(playlist)
        // Needed to said to recycler view we have new data
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val state: TextView = itemView.findViewById(R.id.name)
        val icon: ImageView = itemView.findViewById(R.id.image);
    }
}
