package com.morgonus.android_project_2020.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.morgonus.android_project_2020.R
import com.morgonus.android_project_2020.spotify.view.SpotifyActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonRecyclerViewLocal.setOnClickListener{ goRecyclerViewLocal()}
        buttonRecyclerViewApi.setOnClickListener{ goRecyclerViewApi()}
    }

    private fun goRecyclerViewLocal(){
        startActivity(Intent(this, RecyclerViewActivity::class.java))
    }

    private fun goRecyclerViewApi(){
        startActivity(Intent(this, SpotifyActivity::class.java))

    }
}